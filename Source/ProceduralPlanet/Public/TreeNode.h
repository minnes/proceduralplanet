// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FastNoise/FastNoise.h"
#include "NoiseInfo.h"

/**
 * 
 */
class PROCEDURALPLANET_API TreeNode
{
public:
	TreeNode();
	TreeNode(const FVector& v1, const FVector& v2, const FVector& v3, const FNoiseInfo& noise);
	//TreeNode(const FVector& v1, const FVector& v2, const FVector& v3, const FNoiseInfo& noise);
	//TreeNode(const FVector& v1, const FVector& v2, const FVector& v3, const FNoiseInfo& noise, FastNoise* noiseGenerator);
	TreeNode(const FVector& v1, const FVector& v2, const FVector& v3, const FNoiseLayers& layers);
	TreeNode(const FVector& v1, const FVector& v2, const FVector& v3, const FVector2D& uv1, const FVector2D& uv2, const FVector2D& uv3, const FNoiseLayers& layers);
	~TreeNode();

	TArray<FVector> GetVertices();
	
	const FVector& GetNormal() const { return normal; }
	void SetNormal(const FVector& n) { normal = n; }

	const FVector& GetTangent() const { return tangent; }
	void SetTangent(const FVector& t) { tangent = t; }
	void SplitNode(int depth, int radius = -1, float roughness = 0.7f, int32 MaxHeight = 10);
	void CalculateHeight(FVector& newVertex, const int32 MaxHeight);
	void AddNoiseToBaseTriangle(int32 radius, int32 MaxHeight);
	void SetNoiseValues(float frequency, int octaves, float lacunarity, float persistence, float strength, FastNoise::NoiseType noiseType = FastNoise::NoiseType::SimplexFractal);
	void GetFractalNoise(const FVector& v, float x, float y, int octaves, float frequency, float lacunarity, float persistence, FastNoise::NoiseType noiseType = FastNoise::Simplex);
	float GetFractalNoise(const FVector& v, const FNoiseInfo& noiseInfo);
	float GetLayeredFractalNoise(const FVector& v);
	void RemoveChilds();
	void GetTriangleInformation(TArray<FVector>& vertices, TArray<FVector>& normals, TArray<int>& triangles, TArray<FVector2D>& uvs);
    void _getTriInfo(TArray<FVector>& vertices, TArray<FVector>& normals, TArray<int>& triangles, TArray<FVector2D>& uvs);
protected:
	TSharedPtr<TreeNode> parentNode;
	TSharedPtr<TreeNode> _leftChild;
	TSharedPtr<TreeNode> _rightChild;

	FVector v1;
	FVector v2;
	FVector v3;
	FVector normal;
	FVector tangent;
	
	FVector2D uv1;
	FVector2D uv2;
	FVector2D uv3;
private:
	float _getNoise2D(float x, float y, float frequency, float persistence, int lacunarity, int octaves);

	FastNoise* noiser;
	//const FNoiseInfo& noiseInfo;
	const FNoiseLayers& noiseLayers;
};
