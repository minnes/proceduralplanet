// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components\ProceduralPlanetComponent.h"
#include "FastNoise/FastNoise.h"
#include "NoiseInfo.h"
#include "ProcPlanet.generated.h"

UCLASS()
class PROCEDURALPLANET_API AProcPlanet : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProcPlanet();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
	UMaterialInterface* material;
	//void OnConstruction(const FTransform& Transform) override;

	/*UFUNCTION(BlueprintCallable, Category = "Noise")
	void SetNoiseValues(float frequency, int octaves, float lacunarity, float persistence);*/
	UFUNCTION(/*BlueprintCallable, CallInEditor,*/ Category = "Noise")
		void SetNoiseValues();
protected:
	UFUNCTION(BlueprintCallable, CallInEditor, Category = "Generation")
	void GeneratePlanet();
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Faces")
		UProceduralPlanetComponent* topFace;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Faces")
		UProceduralPlanetComponent* bottomFace; 	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Faces")
		UProceduralPlanetComponent* negXFace; 	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Faces")
		UProceduralPlanetComponent* negYFace; 	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Faces")
		UProceduralPlanetComponent* posXFace; 	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Faces")
		UProceduralPlanetComponent* posYFace;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sea")
		UStaticMeshComponent* seaMesh;
	FastNoise* noiser;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Noise")
		float Roughness = 0.5f;	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Generation")
		float Radius = 1000.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dimensions")
		int32 MaxHeight = 1000;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LOD")
		UCurveFloat* lodCurve;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Colors")
		TArray<FColor> colors;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Colors")
		int32 gammaDivisor; 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Colors")
		int32 baseGamma;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Colors")
		int32 topGamma;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sea")
		UMaterialInterface* seaMaterial;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sea", meta = (ClampMin = "1", ClampMax = "1000000", UIMin = "1", UIMax = "1000000"))
		float seaLevel;
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
private:
	//utility array to loop on every face
	TArray<UProceduralPlanetComponent*> _faces;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LOD", meta = (AllowPrivateAccess = true))
	int initialDepth = 4;

	UPROPERTY(EditAnywhere, Category = "LOD")
		float maxLOD = 14;
	UPROPERTY(EditAnywhere, Category = "LOD")
		float maxDistance = 100000;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Noise | Data", meta = (AllowPrivateAccess = true))
	FNoiseInfo noiseInfo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Noise | Data", meta = (AllowPrivateAccess = true))
		FNoiseLayers noiseLayers;
};
