// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "CoreMinimal.h"
#include "FastNoise/FastNoise.h"
#include "NoiseInfo.generated.h"

USTRUCT(BlueprintType)
struct FNoiseInfo {
	GENERATED_BODY()
		FNoiseInfo(): exp(1.f), octaves(4), lacunarity(2.f), persistence(0.5f), roughness(0.001f), strength(0.f), centerOffset(FVector::ZeroVector) {};

	FNoiseInfo(const FastNoise* fastNoise) {
		exp = 1;
		octaves = fastNoise->GetFractalOctaves();
		lacunarity = fastNoise->GetFractalLacunarity();
		persistence = fastNoise->GetFractalGain();
		roughness = fastNoise->GetFrequency();
		strength = 0.f;
		centerOffset = FVector::ZeroVector;
	};
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data", meta = (ClampMin = "0.01", ClampMax = "10", UIMin = "0.01", UIMax = "10"))
		float exp;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data", meta = (ClampMin = "0", ClampMax = "30", UIMin = "0", UIMax = "30"))
		int octaves;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data", meta = (ClampMin = "1", ClampMax = "30", UIMin = "1", UIMax = "30"))
		float lacunarity;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data", meta = (ClampMin = "0", ClampMax = "1", UIMin = "0", UIMax = "1"))
		float persistence;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data", meta = (ClampMin = "0.0001", ClampMax = "10", UIMin = "0.0001", UIMax = "10"))
		float roughness;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data", meta = (ClampMin = "-1", ClampMax = "1", UIMin = "-1", UIMax = "1"))
		float strength;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		FVector centerOffset;

};

USTRUCT(BlueprintType) 
struct FNoiseLayers {
	GENERATED_BODY()
		FNoiseLayers() {
		layers = { FNoiseInfo() };
	}
	FNoiseLayers(const FNoiseInfo& noise) {
		layers.Add(noise);
		bIsMask = false;
		layerMaskId = 0;
	}
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		TArray<FNoiseInfo> layers;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
	bool bIsMask = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data", meta = (ClampMin = "0", ClampMax = "1000", UIMin = "0", UIMax = "1000"))
		int32 layerMaskId = 0;
};