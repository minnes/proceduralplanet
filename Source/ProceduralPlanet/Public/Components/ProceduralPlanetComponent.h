// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/PrimitiveComponent.h"
#include "TreeNode.h"
#include "NoiseInfo.h"
#include "ProceduralMeshComponent/Public/ProceduralMeshComponent.h"
#include "ProceduralPlanetComponent.generated.h"


/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PROCEDURALPLANET_API UProceduralPlanetComponent : public UPrimitiveComponent
{
	GENERATED_BODY()
public:
	UProceduralPlanetComponent();
	void InitializeComponent() override;
	void TickComponent(float DeltaTime,
		enum ELevelTick TickType,
		FActorComponentTickFunction* ThisTickFunction) override;
	UFUNCTION(BlueprintCallable, Category = "Generation")
		void InitBinaryTree();
	
	UFUNCTION(BlueprintCallable, Category = "Generation")
	void UpdateLODRoam();
	UFUNCTION(BlueprintCallable, Category = "Generation")
		void GenerateMeshLOD0(int depth, const FNoiseInfo& noise);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dimensions")
		int32 Radius = 1000;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dimensions")
		int32 MaxHeight = 1000;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Coordinates")
		FVector leftTopCorner;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Coordinates")
		FVector leftBottomCorner;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Coordinates")
		FVector rightTopCorner;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Coordinates")
		FVector rightBottomCorner;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		UMaterialInterface* material;

	//local axes
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "coordinates")
		FVector _axisA; 
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "coordinates")
		FVector _axisB;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "coordinates")
		FVector normal;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LOD")
		int InitialDepth = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LOD")
		int maxDepth = 13;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LOD")
		UCurveFloat* distanceLODCurve;
	static FastNoise* GetFastNoise();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Noise")
		float roughness = 0.5f;;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Noise")
		float strength = 1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Noise")
		FNoiseLayers NoiseLayers;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Colors")
		TArray<FColor> colors;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Colors")
		int32 GammaDivisor;
	void SetNoiseParameters(float frequency, int octaves, float lacunarity, float persistence, float str);
	void SetLODParameters(UCurveFloat* lodCurve, int maxLOD, int maxDistance);
protected:
	UPROPERTY(BlueprintReadOnly, Category = "Mesh")
		TArray<FVector> vertices;
	UPROPERTY(BlueprintReadOnly, Category = "Mesh")
		TArray<int32> triangles;
	UPROPERTY(BlueprintReadOnly, Category = "Mesh")
		TArray<FVector> normals;
	UPROPERTY(BlueprintReadOnly, Category = "Mesh")
		TArray<FVector2D> uvs;
	UFUNCTION(Category = "Mesh")
	void _generateMesh();

	UFUNCTION()
		void _updateTriangleTree(int treeDepth, int r);
	UPROPERTY()
		int _treeDepth;

	UPROPERTY(EditAnywhere, Category = "LOD")
		float maxLOD = 18;
	UPROPERTY(EditAnywhere, Category = "LOD")
		float maxDistance = 10000;

	float _xCoefficientDepth;

	TSharedPtr<TreeNode> _leftTriNode;
	TSharedPtr<TreeNode> _rightTriNode;

	UProceduralMeshComponent* _mesh;
	void _storeMeshInformation(TArray<FVector>& vertices, TArray<FVector>& normals, TArray<int>& triangles, TArray<FVector2D>& uvs);

	void _roundVertex(FVector& vertex);
	FastNoise fastNoise;

	/*color generations*/

	FColor _getColor(const FVector& v, int32 maxHeight);

};

