// Copyright Epic Games, Inc. All Rights Reserved.

#include "ProceduralPlanet.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ProceduralPlanet, "ProceduralPlanet" );
