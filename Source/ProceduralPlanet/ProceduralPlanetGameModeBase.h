// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ProceduralPlanetGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PROCEDURALPLANET_API AProceduralPlanetGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
