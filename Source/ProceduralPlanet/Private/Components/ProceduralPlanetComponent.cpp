// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/ProceduralPlanetComponent.h"
#include "Kismet\GameplayStatics.h"
#include "ProceduralMeshComponent\Public\KismetProceduralMeshLibrary.h"

UProceduralPlanetComponent::UProceduralPlanetComponent() {
	_mesh = NewObject<UProceduralMeshComponent>(this, "PlanetPatch1");
	_mesh->AttachToComponent(this, FAttachmentTransformRules::KeepRelativeTransform);

	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = true;
}
void UProceduralPlanetComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	UpdateLODRoam();

}
void UProceduralPlanetComponent::InitializeComponent()
{
	Super::InitializeComponent();
	
}
void UProceduralPlanetComponent::InitBinaryTree() {
	_axisA = FVector(normal.Y, normal.Z, normal.X);
	_axisB = FVector::CrossProduct(normal, _axisA);
	FVector upLocalOffset = normal * Radius;
	FVector xLocalOffset = _axisA * Radius;
	FVector yLocalOffset = _axisB * Radius;
	leftTopCorner = -xLocalOffset + yLocalOffset + upLocalOffset;
	leftBottomCorner = -xLocalOffset - yLocalOffset + upLocalOffset;
	rightBottomCorner = xLocalOffset - yLocalOffset + upLocalOffset;
	rightTopCorner = xLocalOffset + yLocalOffset + upLocalOffset;
	_xCoefficientDepth = maxLOD / maxDistance;
	FVector2D uvLB(0, 0);
	FVector2D uvLT(0, 1);
	FVector2D uvRB(1, 0);
	FVector2D uvRT(1, 1);
	_leftTriNode = MakeShared<TreeNode>(rightBottomCorner, leftBottomCorner, leftTopCorner, uvRB, uvLB, uvLT, FNoiseInfo(&fastNoise));
	_rightTriNode = MakeShared<TreeNode>(leftTopCorner, rightTopCorner, rightBottomCorner, uvLT, uvRT, uvRB, FNoiseInfo(&fastNoise));

	//leftNode
	vertices.Append(_leftTriNode->GetVertices());
	FVector n = _leftTriNode->GetNormal();
	normals.Append({ n,n,n });
	triangles.Append({ 0, 1, 2 });
	uvs.Append({ uvRB, uvLB, uvLT });
	//rightNode
	vertices.Append(_rightTriNode->GetVertices());
	n = _rightTriNode->GetNormal();
	normals.Append({ n,n,n });
	triangles.Append({ 3,4,5 });
	uvs.Append({ uvLT, uvRT, uvRB });
	TArray<FColor> vertexColors;
	TArray<FProcMeshTangent> tangents;
	_mesh->CreateMeshSection(0, vertices, triangles, normals, uvs, vertexColors, tangents, true);
	_mesh->SetMaterial(0, material);
}

void UProceduralPlanetComponent::UpdateLODRoam()
{
	APlayerController* plController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	const FVector playerLocation = plController->GetPawn()->GetActorLocation();
	FVector componentLocation = GetComponentLocation() + normal * Radius;
	float distanceFromPawn = (playerLocation - componentLocation).Size();
	int newDepth = distanceLODCurve->GetFloatValue(distanceFromPawn / maxDistance) + maxLOD;
	if (newDepth != _treeDepth) {
		_treeDepth = newDepth;
		if (_treeDepth < InitialDepth) {
			_treeDepth = InitialDepth;
		}
		UE_LOG(LogTemp, Log, TEXT("treeDepth: %d, - distance: %f"), _treeDepth, distanceFromPawn);
		AsyncTask(
			ENamedThreads::AnyBackgroundHiPriTask, [this]() {
				_updateTriangleTree(_treeDepth, Radius);
				_generateMesh();
			});
	}
}

void UProceduralPlanetComponent::GenerateMeshLOD0(int depth, const FNoiseInfo& noise)
{
	_axisA = FVector(normal.Y, normal.Z, normal.X);
	_axisB = FVector::CrossProduct(normal, _axisA);
	FVector upLocalOffset = normal * Radius;
	FVector xLocalOffset = _axisA * Radius;
	FVector yLocalOffset = _axisB * Radius;
	leftTopCorner = -xLocalOffset + yLocalOffset + upLocalOffset;
	leftBottomCorner = -xLocalOffset - yLocalOffset + upLocalOffset;
	rightBottomCorner = xLocalOffset - yLocalOffset + upLocalOffset;
	rightTopCorner = xLocalOffset + yLocalOffset + upLocalOffset;
	FVector2D uvLB(0, 0);
	FVector2D uvLT(0, 1);
	FVector2D uvRB(1, 0);
	FVector2D uvRT(1, 1);
	_roundVertex(leftTopCorner);
	_roundVertex(leftBottomCorner);
	_roundVertex(rightBottomCorner);
	_roundVertex(rightTopCorner);
	_xCoefficientDepth = maxLOD / maxDistance;

	auto leftNode = _leftTriNode;
	auto rightNode = _rightTriNode;
	AsyncTask(ENamedThreads::AnyNormalThreadNormalTask, [leftNode, rightNode]() {
		//reset the treeNode
		if (leftNode != nullptr) {
			leftNode->RemoveChilds();
		}
		if (rightNode != nullptr) {
			rightNode->RemoveChilds();
		}
	});
	//create the new treeNode
	_leftTriNode = MakeShared<TreeNode>(rightBottomCorner, leftBottomCorner, leftTopCorner, uvRB, uvLB, uvLT, NoiseLayers);
	_rightTriNode = MakeShared<TreeNode>(leftTopCorner, rightTopCorner, rightBottomCorner, uvLT, uvRT, uvRB, NoiseLayers);
	AsyncTask(
		ENamedThreads::AnyBackgroundHiPriTask, [this, depth]() {
			_updateTriangleTree(depth, Radius);
			_generateMesh();
		});
}

void UProceduralPlanetComponent::SetNoiseParameters(float frequency, int octaves, float lacunarity, float persistence, float str)
{
	if (_leftTriNode != nullptr) {
		_leftTriNode->SetNoiseValues(frequency, octaves, lacunarity, persistence, str);
	}
	if (_rightTriNode != nullptr) {
		_rightTriNode->SetNoiseValues(frequency, octaves, lacunarity, persistence, str);
	}
}

void UProceduralPlanetComponent::SetLODParameters(UCurveFloat* lodCurve, int lod, int distance)
{
	distanceLODCurve = lodCurve;
	maxLOD = lod;
	maxDistance = distance;
}

void UProceduralPlanetComponent::_generateMesh()
{
	vertices.Empty();
	normals.Empty();
	triangles.Empty();
	uvs.Empty();
	_storeMeshInformation(vertices, normals, triangles, uvs);
	TArray<FColor> vertexColors;
	TArray<FProcMeshTangent> tangents;
	UKismetProceduralMeshLibrary::CalculateTangentsForMesh(vertices, triangles, uvs, normals, tangents);
	vertexColors.SetNumUninitialized(vertices.Num());
	for (int i = 0; i < vertices.Num(); ++i) {
		vertexColors[i] = _getColor(vertices[i], GammaDivisor);
	}

	TFunction<void()> updateSection = [this, vertexColors, tangents]() {

		_mesh->CreateMeshSection(0, vertices, triangles, normals, uvs, vertexColors, tangents, true);
		_mesh->SetMaterial(0, material);
		UE_LOG(LogTemp, Log, TEXT("generateMesh() COMPLETED for %s"), *this->GetName());
	};

	if (!IsInGameThread()) {
		AsyncTask(ENamedThreads::GameThread, updateSection);
	}
	else {
		updateSection();
	}
}

void UProceduralPlanetComponent::_updateTriangleTree(int depth, int r)
{
	if (depth > maxLOD) {
		depth = maxLOD;
	}
	_leftTriNode->AddNoiseToBaseTriangle(Radius, r);
	_rightTriNode->AddNoiseToBaseTriangle(Radius, r);

	_leftTriNode->SplitNode(depth, r, roughness, MaxHeight);
	_rightTriNode->SplitNode(depth, r, roughness, MaxHeight);
}

void UProceduralPlanetComponent::_storeMeshInformation(TArray<FVector>& verts, TArray<FVector>& norms, TArray<int>& tris, TArray<FVector2D>& uv)
{
	_leftTriNode->GetTriangleInformation(verts, norms, tris, uv);
	_rightTriNode->GetTriangleInformation(verts, norms, tris, uv);
}

void UProceduralPlanetComponent::_roundVertex(FVector& vertex)
{
	/*FVector v = FVector(vertex.X, vertex.Y, vertex.Z) * 2.f / Radius - FVector::OneVector;
	float x2 = v.X * v.X;
	float y2 = v.Y * v.Y;
	float z2 = v.Z * v.Z;
	FVector s;
	s.X = v.X * FMath::Sqrt(1.f - y2 / 2.f - z2 / 2.f + y2 * z2 / 3.f);
	s.Y = v.Y * FMath::Sqrt(1.f - x2 / 2.f - z2 / 2.f + x2 * z2 / 3.f);
	s.Z = v.Z * FMath::Sqrt(1.f - x2 / 2.f - y2 / 2.f + x2 * y2 / 3.f);
	vertex = s;*/
	vertex.Normalize();
	vertex = (FVector(1, 1, 1) + vertex) * Radius - FVector(Radius);
}

FColor UProceduralPlanetComponent::_getColor(const FVector& v, int32 maxHeight) {
	float length = v.Size();
	int numColors = colors.Num() - 1;
	int cIdx = length / maxHeight;
	cIdx = cIdx <= numColors ? cIdx : numColors;
	return colors[cIdx];
}