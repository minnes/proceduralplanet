// Fill out your copyright notice in the Description page of Project Settings.


#include "ProcPlanet.h"

// Sets default values
AProcPlanet::AProcPlanet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	USceneComponent* rootFaces = CreateDefaultSubobject<USceneComponent>("RootFaces");
	RootComponent = rootFaces;
	colors = { FColor::Blue, FColor::Green, FColor(212, 123, 74), FColor(181, 199, 199) };
	gammaDivisor = Radius;
	topFace = CreateDefaultSubobject<UProceduralPlanetComponent>("TopFace");
	bottomFace = CreateDefaultSubobject<UProceduralPlanetComponent>("BottomFace");
	negXFace = CreateDefaultSubobject<UProceduralPlanetComponent>("NegXFace");
	negYFace = CreateDefaultSubobject<UProceduralPlanetComponent>("NegYFace");
	posXFace = CreateDefaultSubobject<UProceduralPlanetComponent>("PosXFace");
	posYFace = CreateDefaultSubobject<UProceduralPlanetComponent>("PosYFace");
	topFace->normal = FVector(0, 0, 1);
	bottomFace->normal = FVector(0, 0, -1);
	negXFace->normal = FVector(-1, 0, 0);
	negYFace->normal = FVector(0, -1, 0);
	posXFace->normal = FVector(1, 0, 0);
	posYFace->normal = FVector(0, 1, 0);
	_faces.Reserve(6);
	_faces.Add(topFace);
	_faces.Add(bottomFace);
	_faces.Add(negXFace);
	_faces.Add(negYFace);
	_faces.Add(posXFace);
	_faces.Add(posYFace);
	for (auto& face : _faces) {
		face->AttachToComponent(RootComponent,FAttachmentTransformRules::KeepRelativeTransform);
	}

	seaMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SeaMesh"));
	seaMesh->AttachTo(GetRootComponent());

	static ConstructorHelpers::FObjectFinder<UStaticMesh>SphereMeshAsset(TEXT("StaticMesh'/Engine/BasicShapes/Sphere.Sphere'"));
	seaMesh->SetStaticMesh(SphereMeshAsset.Object);
	seaMesh->GetStaticMesh()->SetMaterial(0, seaMaterial);
	seaMesh->SetRelativeScale3D(FVector(seaLevel));
}
//
//void AProcPlanet::SetNoiseValues(float frequency, int octaves, float lacunarity, float persistence)
//{
//	for (auto face : _faces) {
//		face->SetNoiseParameters(frequency, octaves, lacunarity, persistence);
//	}
//}



//void AProcPlanet::OnConstruction(const FTransform& Transform)
//{
//	Super::OnConstruction(Transform);
//	GeneratePlanet();
//}

void AProcPlanet::SetNoiseValues()
{
	for (auto face : _faces) {
		face->SetNoiseParameters(noiseInfo.exp, noiseInfo.octaves, noiseInfo.lacunarity, noiseInfo.persistence, noiseInfo.strength);
	}
	GeneratePlanet();
}

void AProcPlanet::GeneratePlanet()
{
	for (auto face : _faces) {
		face->material = material;
		face->roughness = noiseInfo.roughness;
		face->strength = noiseInfo.strength;
		face->Radius = Radius;
		face->MaxHeight = MaxHeight;
		face->NoiseLayers = noiseLayers;
		face->SetLODParameters(lodCurve, maxLOD, maxDistance);
		face->GenerateMeshLOD0(initialDepth, noiseInfo);
		face->colors = colors;
		face->GammaDivisor = gammaDivisor;
	}
	seaMesh->SetRelativeScale3D(FVector(seaLevel));
}

// Called when the game starts or when spawned
void AProcPlanet::BeginPlay()
{
	Super::BeginPlay();
	GeneratePlanet();
}

// Called every frame
void AProcPlanet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

