// Fill out your copyright notice in the Description page of Project Settings.


#include "TreeNode.h"

//
//TreeNode::TreeNode(const FVector& v1, const FVector& v2, const FVector& v3, const FNoiseInfo& noise) :v1(v1), v2(v2), v3(v3), tangent(FVector(0, 0, 1)), noiseInfo(noise) {
//	normal = FVector::CrossProduct(v2 - v1, v2 - v3);
//	normal.Normalize();
//	_leftChild = nullptr;
//	_rightChild = nullptr;
//	noiser = new FastNoise();
//	noiser->SetNoiseType(FastNoise::PerlinFractal);
//	noiser->SetFrequency(noiseInfo.frequency);
//	noiser->SetFractalOctaves(noiseInfo.octaves);
//	noiser->SetFractalLacunarity(noiseInfo.lacunarity);
//	noiser->SetFractalGain(noiseInfo.roughness);
//
//}
//
//
//TreeNode::TreeNode(const FVector& v1, const FVector& v2, const FVector& v3, const FNoiseInfo& noise, FastNoise* noiseGenerator) : v1(v1), v2(v2), v3(v3), tangent(FVector(0, 0, 1)), noiseInfo(noise)
//{
//	normal = FVector::CrossProduct(v2 - v1, v2 - v3);
//	normal.Normalize();
//	_leftChild = nullptr;
//	_rightChild = nullptr;
//	noiser = noiseGenerator;
//	FNoiseLayers layers();
//	noiseLayers = &layers;
//}
TreeNode::TreeNode(const FVector& v1, const FVector& v2, const FVector& v3, const FNoiseInfo& noise) : v1(v1), v2(v2), v3(v3), tangent(FVector(0, 0, 1)), noiseLayers(noise)
{
	normal = FVector::CrossProduct(v2 - v1, v2 - v3);
	normal.Normalize();
	_leftChild = nullptr;
	_rightChild = nullptr;
}
TreeNode::TreeNode(const FVector& v1, const FVector& v2, const FVector& v3, const FNoiseLayers& layers) : v1(v1), v2(v2), v3(v3), tangent(FVector(0, 0, 1)), noiseLayers(layers)
{
	normal = FVector::CrossProduct(v2 - v1, v2 - v3);
	normal.Normalize();
	_leftChild = nullptr;
	_rightChild = nullptr;
}

TreeNode::TreeNode(const FVector& v1, const FVector& v2, const FVector& v3, const FVector2D& uv1, const FVector2D& uv2, const FVector2D& uv3, const FNoiseLayers& layers) : v1(v1), v2(v2), v3(v3), uv1(uv1), uv2(uv2), uv3(uv3), tangent(FVector(0, 0, 1)), noiseLayers(layers)
{
	normal = FVector::CrossProduct(v2 - v1, v2 - v3);
	normal.Normalize();
	_leftChild = nullptr;
	_rightChild = nullptr;
}


TreeNode::~TreeNode()
{
}

TArray<FVector> TreeNode::GetVertices()
{
	return TArray<FVector>({ v1, v2, v3 });
}

void TreeNode::SplitNode(int depth, int radius, float roughness, int32 MaxHeight)
{
	if (depth == 0) {
		//RemoveChilds();
		return;
	}
	depth--;
	if (_leftChild == nullptr || _rightChild == nullptr) {
		FVector newVertex = (v1 + v3) / 2;
		if (radius != -1) {
			newVertex.Normalize();
			newVertex = (FVector(1, 1, 1) + newVertex) * radius - FVector(radius);
		}
		//newVertex /= -newVertex;
		CalculateHeight(newVertex, radius);
		//newVertex *= radius;
		const FVector2D newUV = (uv1 + uv3) / 2;
		if (_leftChild == nullptr) {
			_leftChild = MakeShared<TreeNode>(v2, newVertex, v1, uv2, newUV, uv1, noiseLayers);
			_leftChild->SplitNode(depth, radius, MaxHeight);
		}
		if (_rightChild == nullptr) {
			_rightChild = MakeShared<TreeNode>(v3, newVertex, v2, uv3, newUV, uv2, noiseLayers);
			_rightChild->SplitNode(depth, radius, MaxHeight);
		}
	}	
}

void TreeNode::CalculateHeight(FVector& newVertex, const int32 MaxHeight)
{
	float elevation = GetLayeredFractalNoise(newVertex / MaxHeight);
	FVector heightIncrement = newVertex / newVertex.Size();
	if (heightIncrement.IsNearlyZero()) {
		check(false);
		UE_LOG(LogTemp, Error, TEXT("heightIncrement versor is nearly zero: %s"), *heightIncrement.ToString());
	}
	UE_LOG(LogTemp, Warning, TEXT("heightIncrement is: %s, newVertex is: %s"), *heightIncrement.ToString(), *newVertex.ToString());
	heightIncrement *=(1 + elevation) * MaxHeight;
	newVertex += heightIncrement;
}

void TreeNode::AddNoiseToBaseTriangle(int32 radius, int32 MaxHeight)
{
	CalculateHeight(v1, radius);
	CalculateHeight(v2, radius);
	CalculateHeight(v3, radius);
}

void TreeNode::SetNoiseValues(float frequency, int octaves, float lacunarity, float persistence, float strength, FastNoise::NoiseType noiseType)
{
	if (noiser == nullptr) {
		UE_LOG(LogTemp, Warning, TEXT("FastNoise not set in the tree"));
		return;
	}
	noiser->SetNoiseType(noiseType);
	noiser->SetFrequency(frequency);
	noiser->SetFractalOctaves(octaves);
	noiser->SetFractalLacunarity(lacunarity);
	noiser->SetFractalGain(persistence);
	if (_leftChild != nullptr) {
		_leftChild->SetNoiseValues(frequency, octaves, lacunarity, persistence, strength, noiseType);
	}
	if (_rightChild != nullptr) {
		_rightChild->SetNoiseValues(frequency, octaves, lacunarity, persistence, strength, noiseType);
	}
}

void TreeNode::GetFractalNoise(const FVector& v, float x, float y, int octaves, float frequency, float lacunarity, float persistence, FastNoise::NoiseType noiseType)
{
	SetNoiseValues(frequency, octaves, lacunarity, persistence, noiseType);
	float noiseZ = noiser->GetNoise(v.X, v.Y);
}
//
//float TreeNode::GetFractalNoise(const FVector& v)
//{
//	float noiseValue = 0;
//	float frequency = noiseInfo.roughness;
//	float amplitude = noiseInfo.lacunarity;
//
//	for (int i = 0; i < noiseInfo.octaves; ++i)
//	{
//		float value = FMath::PerlinNoise3D(v * frequency /* todo add offset*/);
//		noiseValue += (value + 1) * .5f * amplitude;
//		frequency *= amplitude;
//		amplitude *= noiseInfo.persistence;
//	}
//
//	noiseValue = noiseValue - noiseInfo.strength;
//	return noiseValue;
//	//return noiser->GetSimplexFractal(v.X, v.Y);
//}

float TreeNode::GetFractalNoise(const FVector& v, const FNoiseInfo& info)
{
	float noiseValue = 0;
	float frequency = info.roughness;
	float amplitude = 1;

	for (int i = 0; i < info.octaves; ++i)
	{
		float value = FMath::PerlinNoise3D(v * frequency + info.centerOffset);
		noiseValue += value * amplitude; //(value + 1) * .5f * amplitude;
		amplitude *= info.persistence;
		frequency *= info.lacunarity;
	}
	noiseValue = 1 - FMath::Abs(noiseValue);
	noiseValue *= noiseValue;
	noiseValue = noiseValue - info.strength;
	noiseValue = FMath::Pow(noiseValue, info.exp);
	return noiseValue;
	//return noiser->GetSimplexFractal(v.X, v.Y);
}
float TreeNode::GetLayeredFractalNoise(const FVector& v) {
	float value = 0;
	for (const FNoiseInfo& layer : noiseLayers.layers) {
		value += GetFractalNoise(v, layer);
	}
	return value;
}
void TreeNode::RemoveChilds()
{
	if (_leftChild != nullptr) {
		_leftChild->RemoveChilds();
		_leftChild = nullptr;
	}
	if (_rightChild != nullptr) {
		_rightChild->RemoveChilds();
		_rightChild = nullptr;
	}
}

void TreeNode::GetTriangleInformation(TArray<FVector>& vertices, TArray<FVector>& normals, TArray<int>& triangles, TArray<FVector2D>& uvs)
{
	if (_leftChild != nullptr) {
		_leftChild->GetTriangleInformation(vertices, normals, triangles, uvs);
	}
	else {
		_getTriInfo(vertices, normals, triangles, uvs);
	}
	if (_rightChild != nullptr) {
		_rightChild->GetTriangleInformation(vertices, normals, triangles, uvs);
	}
	else {
		_getTriInfo(vertices, normals, triangles, uvs);
	}
}

void TreeNode::_getTriInfo(TArray<FVector>& vertices, TArray<FVector>& normals, TArray<int>& triangles, TArray<FVector2D>& uvs)
{
	int numVerts = vertices.Num();
	vertices.Append(GetVertices());
	normals.Append({ normal, normal, normal });
	triangles.Append({ numVerts, ++numVerts, ++numVerts });
	uvs.Append({ uv1, uv2, uv3 });
}
